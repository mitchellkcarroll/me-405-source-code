
'''
@file change_getter.py

'''


def getChange(price,payment):
    '''
    @
    '''
    
    vals = (1,5,10,25,100,500,1000,2000)
    pay_val = 0
    
    for n in range(len(vals)):
        pay_val += vals[n]*payment[n]
    change = pay_val - price*100
    
    if change < 0:
        return None
        
    else:
        idx = True
        n=1
        chg = []
        while idx == True:
            test = vals[len(vals)-n]
            if n == len(vals)+1:
                idx = False
            elif change < test:
                chg.insert(0,0)
                n+=1
            elif change >= test:
                num = int(change/test)
                chg.insert(0,num)
                change = change - num*test
                n+=1
        change_den = (chg[0],chg[1],chg[2],chg[3],chg[4],chg[5],chg[6],chg[7])
        return change_den