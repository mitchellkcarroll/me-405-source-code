'''
@file coms_driver_pc.py
@brief A driver for MCU communicaiton on the PC side
@author Mitchell Carroll
'''

import serial

class coms_driver:
    '''
    A communicaitons driver
    
    This driver is capable of checking if data is available, reading, and sending serial data
    via COM3 to an MCU
    '''
    def __init__(self):
        '''
        Creates a coms driver object

        '''
        
        ## sets up serial communication with com3
        self.ser = serial.Serial(port = 'COM7', baudrate = 115273,timeout = 1)
    def check(self):
        '''
        A method that checks if data is available
        '''
        if self.ser.inWaiting() != 0:
            return True
        else:
            return False
    def send(self,msg):
        '''
        A method that sends data

        '''
        self.ser.write(str(msg).encode('ascii'))
        
    def read(self):
        '''
        A method that reads data
        '''
        val = self.ser.readline()
        return val
    def close(self):
        '''
        A method that closes serial coms

        '''
        self.ser.close()
        