'''
@file touch_driver.py
@brief A driver that interacts with a resistive touch pannel
@details This driver allows the user to input in objects for the pins that 
are being used with the pannel in addition to the physical dimensions of the pannel
this class contains several methods for getting position. one for each direction
x and y and one for z which returns a boolean that is True if the pannel is 
being touched. get_pos() returns a tuple that contains all position values.
@author Mitchell Carroll
'''

from pyb import ADC,Pin
import utime

class touch_driver:
    '''
    A class that interfaces with a resistive touch pannel
    '''
    def __init__(self,xp,xm,yp,ym,dx,dy):
        '''
        Creates a touch driver object tha takes the inputs of four pin objects
        and two dimensions for the touch panel
        @param xp The pin object for x+
        @param xm The pin object for x-
        @param yp The pin object for y+
        @param ym The pin object for y-
        @param dx The width of the pannel in th x direction
        @param dy The length of the pannel in the y direction
        '''
        
        ## The pin object for x+
        self.xp = xp
        
        ## The pin object for x-
        self.xm = xm
        
        ## The pin object for y+
        self.yp = yp
        
        ## The pin object for y-
        self.ym = ym
        
        ## The width of the pannel in th x direction
        self.dx = dx
        
        ## The width of the pannel in th x direction
        self.dy = dy
        
        ## an integer that is the maximum range in analog values
        self.cal = 4000
        
        ## The reference for mm/ohm
        self.dist = max(dx,dy)
    def get_x(self):
        '''
        A method that returns the current X reading from the pannel in mm
        '''
        utime.sleep_us(10)
        self.xp.init(mode = Pin.OUT_PP,value = 1)
        self.xm.init(mode = Pin.OUT_PP,value = 0)
        self.yp.init(mode = Pin.IN)
        self.ym.init(mode = Pin.IN)
        
        ADCx = ADC(self.ym)
        
        read = ADCx.read()
        
        xpos = read*self.dist/self.cal - self.dx/2
        
        return xpos

    def get_y(self):
        '''
        A method that returns the Y reading from the panel in mm
        '''
        utime.sleep_us(10)
        self.xp.init(mode = Pin.IN)
        self.xm.init(mode = Pin.IN)
        self.yp.init(mode = Pin.OUT_PP, value = 1)
        self.ym.init(mode = Pin.OUT_PP, value = 0)
        
        ADCy = ADC(self.xm)
        
        read = ADCy.read()
        
        ypos = read*self.dist/self.cal - self.dy/2
        
        return ypos
    
    def get_z(self):
        '''
        A method that returns a logic value for whether of not the pannel is 
        currently being touched
        '''
        utime.sleep_us(10)
        self.xp.init(mode = Pin.IN)
        self.xm.init(mode = Pin.OUT_PP, value = 0)
        self.yp.init(mode = Pin.OUT_PP, value = 1)
        self.ym.init(mode = Pin.IN)
        
        ADCz1 = ADC(self.ym)
        ADCz2 = ADC(self.xp)
        
        read1 = ADCz1.read()
        read2 = ADCz2.read()
        
        if (read1  >= 4050 or read1 <= 200) or (read2 >= 4050 or read2 <= 200):
            
            return False
        else:
            return True
        
    def get_pos(self):
        '''
        A method that returns a tuple of all position readings (x,y,z)
        '''
        x = self.get_x()
        y = self.get_y()
        z = self.get_z()
        
        data = (x,y,z)
        
        return data