function [xdot] = platform_openloop(J_x,J_u,t,x,u)
if t > 0.01
    u=0;
end
xdot = J_x*x + J_u*u;

end