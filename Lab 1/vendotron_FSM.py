'''
@file vendotron_FSM.py
@brief A vending machine script
@details This file contains a finite state machine that simulates the function of 
a vending machine. 

Below is an FSM diagram for the system:
    
@image html vendotron_FSM.png "Vendotron FSM diagram" width=600px

@author Mitchell Carroll
'''

import time
from kyb_driver import update
import shares_lab1
from change_getter import getChange

## The initial state of the system
S0_init = 0

## The waiting state for the system
S1_wait = 1

## In this state the balance is shown
S2_bal = 2

## In this state the price is shown
S3_price = 3

## In this state the seletion is vended
S4_vend = 4

## In this state the change is returned
S5_return = 5

class task_vendotron:
    '''
    @brief A vending machine task
    '''
    
    def __init__(self,interval):
        '''
        @brief Creates a task_vendotron object
        @param interval specifies the amount of time between runs
        '''
        
        ## Specifies the amount of time between runs
        self.interval = interval
        
        ## Specifies the state of the system
        self.state = S0_init
        
        ## Records the time the constructor is run
        self.start_time = time.time()
        
        ## Records the running balance in the vending machine
        self.money = 0
        
        ## Records the next time the script will run
        self.next_time = self.start_time + self.interval
        
        ## specifies which options are available for soda choices
        self.options = ['c','s','p','d']
        
        ## Specifies the price of the choices
        self.price = [100,85,120,110]
        
        ## Specifies the name of the different choices
        self.name = ['Cuke','Spryte','Popsi','Dr. Pupper']
        
        ## A variable that contains the selected soda
        self.choice = None
        
        ## A boolean that is true if the balance will be shown
        self.show = True
        
        ## A boolean that is true if the coins should be ejected
        self.eject = False
        
        ## A variable that contains the index for the selected choice
        self.idx = None
        
    def run(self,pushed_key):
        '''
        @brief runs one iteration of the task
        @param pushed_key records the key that has been pressed
        '''
        
        ## A variable that contains the key that has been pushed
        self.pushed_key = pushed_key
        if self.pushed_key:
            
            ## records the deciphered keypress
            self.key = update(self.money,self.pushed_key)
            if isinstance(self.key,int):
                self.money = self.key
                self.show = True

            elif self.key in self.options:
                self.choice = self.key

            elif self.key == 'e':
                self.eject = True

            else:
                print('Wrong Button Dummy')
            shares_lab1.pushed_key = None  
            
        ## records the current time of the run    
        self.curr_time = time.time()
        if self.curr_time >= self.next_time:
            
            if self.state == S0_init:
                print('Starting Up')
                self.transitionto(S1_wait)
                print('Give me money or pick something')
                
            elif self.state == S1_wait:
                if self.money != 0:
                    self.show = True
                    self.transitionto(S2_bal)
                elif self.choice:
                    self.transitionto(S3_price)
                    
            elif self.state == S2_bal:
                if self.show:
                    print('Balance: ' + str(self.money/100))
                    self.show = False
                    
                if self.choice:
                    self.transitionto(S3_price)
                    
                if self.eject:
                    self.transitionto(S5_return)
                    self.eject = False
            elif self.state == S3_price:
                
                self.idx = self.options.index(self.choice)
                print(self.name[self.idx] + ': ' + str(self.price[self.idx]/100))
                
                ## recods the tuple containing your change
                self.change = getChange(self.price[self.idx],self.money)
                if self.change:
                    self.transitionto(S4_vend)
                    
                else:
                    print('more money dummy')
                    self.show = True
                    self.transitionto(S2_bal)
                self.choice = None
                
            elif self.state == S4_vend:
                print('grab your ' + self.name[self.idx])
                self.transitionto(S5_return)
                
            elif self.state == S5_return:
                print('your change is: ' + str(self.change))
                self.money = 0
                self.transitionto(S1_wait)
                print('Give me money or pick something')
            
            self.next_time = self.curr_time + self.interval
    def transitionto(self,newstate):
        '''
        @brief A method that changes the state of the system
        '''
        self.state = newstate