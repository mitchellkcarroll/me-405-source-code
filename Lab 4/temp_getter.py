'''
@file temp_getter.py
@brief A script that collects and records temperature data
@details This script uses the mcp9808 driver to read temperatures from an external
temperature sensor. Temperatures are also read from the MCU core temperature sensor.
Readings are taken every 60 seconds and recorded in a csv along with a timestamp.
@author Mitchell Carroll
'''

from mcp9808 import I2C_driver
import utime
import pyb

## An i2c driver object
i2c = I2C_driver(1,0x18)

## an ADC object
a = pyb.ADCAll(12, 0x70000)

a.read_vref()

## Records teh time that the script starts
start_time = utime.ticks_ms()

## A variable that contains a list of collected data
data = []

## An LED object
led = pyb.Pin(pyb.Pin.cpu.A5,pyb.Pin.OUT)

## Specifies the time the scrip will stop taking data
end = utime.ticks_add(8*60*60*1000,start_time)

## Records the current time for the run
curr_time = start_time


while True:
    if curr_time <= end:
        curr_time = utime.ticks_add(start_time,utime.ticks_ms())
        
        ## A variable that contains the timestamp for the reading
        time = utime.ticks_diff(utime.ticks_ms(),start_time)/1000
        
        ## The core temperature at a the time of the last reading
        core_temp = a.read_core_temp()
        
        ## The ambient temperature at the time of the last reading
        amb_temp = i2c.celcius()
        
        data.append([time,core_temp,amb_temp])
    
        print(data[len(data)-1])
    
        utime.sleep(60)
        
        with open ("temp.csv", "w") as a_file:
            for n in range(len(data)):
                
                ## The line that will be added to the CSV each iteration
                line = str(data[n][0]) + ',' + str(data[n][1]) + ',' + str(data[n][2])
                a_file.write("{:}\r\n".format (line))
                
    else:
        led.on
       
