'''
@file mcp9808.py
@brief A driver for the mcp9808 temperature sensor
@details This script is used for communication with an mcp9808 temperature sensor.
This sensor used I2C communication. the master reads teh temperature memory register
and has several methods that return te contents of the register in various units.
This script also contains a method to check if an I2C device is active on the 
initialized address.
@author Mitchell Carroll
'''

from pyb import I2C

class I2C_driver:
    '''
    A driver class for an mcp9808 temperature sensor
    '''
    def __init__(self,bus,addr):
        '''
        Creates an I2C_driver object
        '''
        
        ## An i2c object on a specified bus
        self.i2c = I2C(bus)
        self.i2c.init(I2C.MASTER)
        
        ## An i2c slave address assigned by the user
        self.addr = addr

    def check(self):
        '''
        A method that checks if a connection is available at a given address
        '''
        conn = self.i2c.is_ready(self.addr)
        return conn
        
    def celcius(self):
        '''
        A method that returns the temperature reading in Celcius
        '''
        buff = bytearray(2)
        data = self.i2c.mem_read(buff,self.addr,5)
        upper = bin(data[0])
        lower = bin(data[1])
        
        temp = 0
        # temp += upper[6]*2**7
        # temp += upper[7]*2**6
        # temp += upper[8]*2**5
        # temp += upper[9]*2**4
        # temp += lower[2]*2**3
        # temp += lower[3]*2**2
        # temp += lower[4]*2**1
        # temp += lower[5]*2**0
        # temp += lower[6]*2**(-1)
        # temp += lower[7]*2**(-2)
        # temp += lower[8]*2**(-3)
        # temp += lower[9]*2**(-4)
        
        
        for n in range(len(upper)-6):
            add=(2**(7-n))
            log = int(upper[n+6])
            temp += add*log
                
        for n in range(len(lower)-2):
            add=(2**(3-n))
            log = int(lower[n+2])
            temp += add*log
        
        
        return temp
        
        
    def farenheit(self):
        '''
        A method that returns the temperature reading in Farenheit
        '''
        temp = self.celcius()
        temp = temp*9/5 +32
        return temp