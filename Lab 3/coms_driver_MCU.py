'''
@file coms_driver_MCU.py
@brief A driver for serial communication with an MCU from the MCU side
@author Mitchell Carroll
'''

from pyb import UART

class coms_driver:
    '''
    A communications 
    
    This driver is capable of checking if data is available, reading, and sending serial data
    via UART(2) to a PC

    '''
    def __init__(self):
        '''
        Creates a coms driver object
    
        '''
        
        ## Sets up UART comunication with the PC
        self.ser = UART(2)
    def check(self):
        '''
        A method that checks if data is available
        '''
        if self.ser.any() != 0:
            return True
        else:
            return False
    def send(self,msg):
        '''
        A method that sends data


        '''
        self.ser.write(msg)
    def read(self):
        '''
        A method that reads data
        '''
        val = self.ser.readline()
        return val