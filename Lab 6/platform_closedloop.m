function [xdot] = platform_closedloop(J_x,J_u,t,x,k)

u = -k*x;

xdot = J_x*x + J_u*u;

end