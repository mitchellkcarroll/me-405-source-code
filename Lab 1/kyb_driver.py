'''
@file kyb_driver.py
@brief A file that interacts with yur keyboard
@details This file contains a method that can be used to recieve non
blocking input from your keyboard. This method will return the amount of money
that has been added to the total when a "coin" has been inserted.
@author Mitchell Carroll
'''


def update(money,pushed_key):
    '''
    @brief A method that returns keyboard input
    '''
    if pushed_key == "0":
        money += 1
        return money
    elif pushed_key == '1':
        money += 5
        return money
    elif pushed_key == '2':
        money += 10
        return money
    elif pushed_key == '3':
        money += 25
        return money
    elif pushed_key == '4':
        money += 100
        return money
    elif pushed_key == '5':
        money += 500
        return money
    elif pushed_key == '6':
        money += 1000
        return money
    elif pushed_key == '7':
        money += 2000
        return money
   
    else:
        return pushed_key
    
    
    

