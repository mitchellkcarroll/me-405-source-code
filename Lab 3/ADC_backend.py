'''
@file ADC_backend.py
@brief A script that records the voltage values during a button press
@details This script waits for a start code from the user and then tunrs on 
an LED and begins sampling data from an analog to digital converter. 
When a button press triggers good data, the LED will turn off. It could take up to 
10 button presses to get good data. The data is the transmitted in batch to a pc.

Below is a finite state diagram:
    
@image html ADC_backend_FSM.png" Analog to Digital Converter Finite State Diagram" width=600px

@author Mitchell Carroll
'''


import pyb
import array
from coms_driver_MCU import coms_driver
import utime
from pyb import UART

## Init state
S0_init = 0

## Wait for command
S1_wait = 1

## Record ADC data
S2_measure = 2

## Send Results
S3_send = 3

## Records the current state of the system
state = 0

while True:
    
    if state == S0_init:
        
        ## A button object tied to the onboard user button
        button = pyb.Pin(pyb.Pin.cpu.C13,pyb.Pin.IN,pyb.Pin.PULL_UP)

        ## An analog to digital converter object tied to pin A0
        ADC = pyb.ADC(pyb.Pin.cpu.A0)
        
        ## An LED object tied to the onboard LED
        led = pyb.Pin(pyb.Pin.cpu.A5,pyb.Pin.OUT_PP)
        
        ## A buffer for the ADC to fill
        data = array.array('H',(0 for index in range(200)))
        
        ## A timer that specifies the frequency that the ADC will sample
        tim = pyb.Timer(6,freq = 50000)
        
        ## A communications driver object
        com = coms_driver()
        
        ## A boolean that is true if data has been sent in the ast 1 seecond
        sent = False
        
        ## A serial communications object
        ser = UART(2)
        
        ## Records the time that measurement will start again after data is sent
        start_time = utime.ticks_ms()
        
        state = S1_wait
        
    elif state == S1_wait:
        
        if sent == False:

            if ser.any() !=0:
                
                ## Records the received character
                val = ser.readchar()
                if val == 71:
                    led.on()
                    state = S2_measure
        
        if sent == True:
            
            ## Records the current time for the run
            curr_time = utime.ticks_ms()
            if utime.ticks_diff(curr_time,start_time)>0:
                sent = False              
                

    elif state == S2_measure:

        ADC.read_timed(data,tim)
        if data[0] < 10 and data[len(data)-1] > 4000:
            led.off()
            
            ## Records the time that data was read
            read_time = utime.ticks_ms()
            start_time = utime.ticks_add(read_time,1000)
            state = S3_send
            
    elif state == S3_send:
        
        com.send(str(data))
        sent = True
        state = S1_wait
        