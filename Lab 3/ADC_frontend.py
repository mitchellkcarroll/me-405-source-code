'''
@file ADC_frontend.py
@brief A task that sends and recieves data
@details This script allows the user to send a start code to a MCU. The MCU
will then record data and send data back in batch. This script then parses
the received data, plots the results and then saves them as a CSV.

The following is a finite state diagram for the system:
    
@image html ADC_frontend_FSM.png" User Interface Finite State Diagram" width=600px

@author Mitchell Carroll
'''

import time
from coms_driver_pc import coms_driver
from matplotlib import pyplot as plt
import numpy as np
import csv

## Specifies how frequently a task will run
interval = 0.1

## Records the time that the task first runs
start_time = time.time()

## records the time that the task will next run
next_time = start_time + interval

## Init state
S0_init = 0

## Send data
S1_send = 1

## Wait for response
S2_wait = 2

## Plot results
S3_plot = 3

## records teh current state of teh system
state = 0

def on_keypress (thing):
    """ Callback which runs when the user presses a key.
    """
    global pushed_key

    pushed_key = thing.name

while True:

    ## Records teh current time for the run
    curr_time = time.time()
    if curr_time>=next_time:
        
        if state == S0_init:
            import keyboard

            ## Stores which key has been pressed
            pushed_key = None


            keyboard.on_press(on_keypress)
            
            ## A communication driver object
            com = coms_driver()
            
            print('Press "G" to start sampling')
            
            state = S1_send
            
        if state == S1_send:
            
            if pushed_key == 'G':
                
                print('you pressed G')
            
                com.send('G')
                state = S2_wait
            
        if state == S2_wait:
            
            myval = com.read()
            if myval:
                
                next_time = curr_time + interval
                
                com.close()
                
                state = S3_plot
                
        if state == S3_plot:
            
            ## Stores the raw serial data
            myval = str(myval).strip('b"array(')
            myval = myval.strip("'H', [")
            myval = myval.strip('])"')
            
            ## Stores the stripped and split serial data
            data = myval.split(', ')
            for n in range(len(data)):
                data[n] = int(data[n])*0.000806
                
            ## Timestamps for the data
            time = np.arange(0,4,0.02).tolist()
            plt.plot(time,data,'k', linestyle = 'dotted')
            plt.xlabel('Time (ms)')
            plt.ylabel('Voltage (V)')
            plt.title('Voltage vs Time for a button press')
            
            ## Stores data that will be exported to csv
            rows = [time,data]
            with open('Data.csv', 'w') as f: 
                                  
                # using csv.writer method from CSV package 
                write = csv.writer(f) 
                             
                write.writerows(rows)
            break
            