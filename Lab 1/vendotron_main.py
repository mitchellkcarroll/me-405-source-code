'''
@file vendotron_main.py
@brief A file that ru12345pns the vending machine tasks
@author Mitchell Carroll
'''

from vendotron_FSM import task_vendotron
import keyboard
import shares_lab1

## the amount of time between runs
interval = 0.4

## the vending machine task
task1 = task_vendotron(interval)

## records the key that has been pressed
shares_lab1.pushed_key = None
def on_keypress (thing):
    """ Callback which runs when the user presses a key.
    """

    shares_lab1.pushed_key = thing.name
keyboard.on_press(on_keypress)

while True:
    task1.run(shares_lab1.pushed_key)