'''
@file plotter.py
@brief A script that reads temperature data from a csv and plots it
@author Mitche Carroll
'''

from matplotlib import pyplot as plt
import csv

with open('temp.csv') as f:
    ## A variable that contains raw csv data
    reader = csv.reader(f)
    
    ## A variable that contains the list form of the data
    data = list(reader)
    
## A list that contains te timestamps of the data
time = []

## A variable that contains the core temperature data
core_temp = []

## A variable that contains the ambient temperature data
amb_temp = []

## A counter for parsing data
n=0
while True:
    if data[n] ==[]:
        data.pop(n)
    else:
        time.append(float(data[n][0])/3600)
        core_temp.append(float(data[n][1]))
        amb_temp.append(float(data[n][2]))
        
        n+=1
    if n == len(data):
        break

core, = plt.plot(time,core_temp,'k',label = 'Core Temperature')
amb, = plt.plot(time,amb_temp,'k',linestyle = '--', label = 'Ambient Temperature')
plt.xlabel('Time (hrs)')
plt.ylabel('Temperature (C)')
plt.title('Temperature vs Time')
plt.legend(handles = [core,amb])
