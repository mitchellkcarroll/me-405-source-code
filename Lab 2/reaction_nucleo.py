'''
@file reaction.py
@brief A reaction time measurer
@details This file measures the reaction time of the user. The script turns on an LED at a random time between 2 and 3 seconds
and begins timing. The user should press the button as soon as possible once the LED is on. When this is done,
it triggers an interupt that stops the timer. This is reapeated until the user presses ctrl + c and the average score is
displayed.
@author Mitchell Carroll
'''

import pyb
import utime
import random

## A boolean that is true when the button has been pressed
button = False

## A variable that counts the number of times reaction time has been tested
count = 0

## Records the time that the button has been pressed
off_time = 0

tim = pyb.Timer(2,prescaler = 1,period = 0x7FFFFFFF)

def handle_interrupt(pin):
    '''
    @brief a method that stops the timer
    '''
    global button
    button = True
    global off_time
    off_time = utime.ticks_us()

## constructs an output on GPIO 2
led = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT)

## constructs an input on GPIO 23
pir = pyb.Pin(pyb.Pin.cpu.C13, pyb.Pin.IN,pyb.Pin.PULL_DOWN)

pir.irq(trigger=pyb.Pin.IRQ_RISING, handler=handle_interrupt)

## Records the time that the program begins running
start_time = utime.ticks_us()

## Specifies the ammount of time that will pass before turning on the LED
delay = random.randrange(2000000,3000000)

## Specifies the time that the LED will turn on
on_time = utime.ticks_add(start_time,delay)

## Records the average reaction time
avg_react = 0

## A boolean that is true if the LED is on
led_idx = False
while True:
    try:
        
        ## records the current time in the loop
        curr_time = utime.ticks_us()
        
        if utime.ticks_diff(curr_time,on_time)>0:
            led.on()
            led_idx = True
            
        if led_idx and button:
            
            ## Records the reaction time for the current test
            react_time = utime.ticks_diff(off_time,on_time)
            if react_time > 10000:
                led.off()
                led_idx = False
                count += 1
                print('Reaction Time: ' + str(react_time/1000000) + ' seconds')
                avg_react = (avg_react*(count-1) + react_time)/count
                delay = random.randrange(2000000,3000000)
                on_time = utime.ticks_add(off_time,delay)
                
            button = False
        if utime.ticks_diff(curr_time,on_time)>1000000:
            led.off()
            led_idx =False
            on_time = utime.ticks_add(off_time,delay)
    except KeyboardInterrupt:
        print('Average Reaction Time: ' + str(avg_react/1000000) + ' seconds')
        break
